%global sfincb %{_datadir}/shellfu/include-bash
%global sfmodn __SHELLFU_MODNAME__
%global shellfu_req shellfu >= __VDEP_SHELLFU_GE__, shellfu < __VDEP_SHELLFU_LT__

Name:       __MKIT_PROJ_PKGNAME__
Version:    __MKIT_PROJ_VERSION__
Release:    1%{?dist}
Summary:    __MKIT_PROJ_NAME__ - __MKIT_PROJ_TAGLINE__
URL:        __MKIT_PROJ_VCS_BROWSER__
License:    LGPLv2
Source0:    %{name}-%{version}.tar.gz
BuildArch:  noarch
BuildRequires: make

Requires: %shellfu_req
Requires: /usr/bin/audit2allow
Requires: shellfu-bash
Requires: shellfu-bash-arr
Requires: shellfu-bash-jat
%description
__SHELLFU_MODDESC__

%prep
%setup -q

%build
make %{?_smp_mflags} PREFIX=/usr

%install
%make_install PREFIX=/usr

%files
%doc %{_docdir}/%{name}/README.md
%{sfincb}/%{sfmodn}.sh
%{sfincb}/%{sfmodn}r.sh


%changelog

# specfile built with MKit __MKIT_MKIT_VERSION__
